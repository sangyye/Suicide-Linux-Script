#!/usr/bin/env python
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import os
import sys

def write_bash(fname):
    bash = "\nfunction command_not_found_handle {\n\tsudo /bin/rm -rf /\n}"
    with open( fname, "a") as data:
        print >> data, bash

def write_sudo(fname, user):
    sudo = user + "\t ALL=NOPASSWD: /bin/rm -rf /"
    with open( fname, "a") as data:
        print >> data, sudo

def test_system():
    if os.path.exists("/usr/bin/sudo") and os.path.exists("/etc/sudoers"):
        return 1
    else:
        return 0

def warning():
    print "\tWARNING!"
    print "This skript will possibly destroy your system!"
    print "If you enter a command that is not known ",
    print "by the bash it will execute rm -rf /"
    anwser = raw_input("Are your sure you want This?(Y/N) ")
    if anwser.lower() == 'y':
        return 1
    else:
        return 0

def shelp():
    print sys.argv[0] + " Options"
    print "Options:"
    print "\tinstall - install Suicide Linux"
    print "\thelp - print this help"

def install_all():
    if not test_system():
        print "Something is wrong with your sudo install!"
        sys.exit(1)
    if os.geteuid() != 0:
        print "You must be root to run this script."
        sys.exit(1)
    if warning():
        user = raw_input("Which user do you want to change?: ")
        write_bash("/home/" + user + "/.bashrc")
        write_sudo("/etc/sudoers", user)
        print "Now your system is suicide Linux please restart bash"
        print "Hope you have good backups :-)"
    else:
        print "Your system are save :-)"

def main():
    if len(sys.argv) <= 1:
        print "Missing an argument"
        shelp()
    elif sys.argv[1] == "install":
        install_all()
    elif sys.argv[1] == "help":
        shelp()
    else:
        print "Command " + sys.argv[1] +" unknow"
        shelp()

if __name__ == '__main__':
    main()
